#ifndef FSDB_H
#define FSDB_H

#include <boost/filesystem.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>

#include <iostream>
#include <shared_mutex>
#include <functional>


#include "FSDB/file_reader.h"


class FSDB
{

public:

//------------------------------------------------------------------------------
    static bool has_collection(const std::string & name)
    {

        std::shared_lock lk(instance().m_mutex);

        const std::string  coll =
                std::string(collections_path).append("/").append(name);
        return boost::filesystem::exists(coll) &&
                boost::filesystem::is_directory(coll);
    }

//------------------------------------------------------------------------------
    static bool new_collection(const std::string & name)
    {
        std::lock_guard lk(instance().m_mutex);

        return make_dir_if_not_exists(
                    std::string(collections_path).append("/").append(name));
    }

//------------------------------------------------------------------------------
    template<typename T>
    static bool get(const std::string & collection,
                const std::string & doc_name,
                const std::string & dotted_path,
                T & value)
    {
        using namespace boost;
        std::shared_lock lk(instance().m_mutex);

        const std::string path(build_doc_path(collection, doc_name));


        if(!filesystem::exists(path))
        {
            return false;
        }

        std::string json_str(get_file(path));

        std::stringstream ss(json_str);

        property_tree::ptree obj;
        property_tree::json_parser::read_json(ss, obj);

        boost::optional<T> res = obj.get_optional<T>(dotted_path);
        if (!res)
        {
            return false;
        }

        value = res.value();
        return true;
    }

//------------------------------------------------------------------------------

    static boost::optional<boost::property_tree::ptree> get_child(
            const std::string & collection,
            const std::string & doc_name,
            const std::string & dotted_path)
    {
        using namespace boost;
        std::shared_lock lk(instance().m_mutex);

        const std::string path(build_doc_path(collection, doc_name));


        if(!filesystem::exists(path))
        {
            return boost::optional<boost::property_tree::ptree>();
        }

        std::string json_str(get_file(path));

        std::stringstream ss(json_str);

        property_tree::ptree obj;
        property_tree::json_parser::read_json(ss, obj);
        auto opt = obj.get_child_optional(dotted_path);
        if(!opt)
        {
            return boost::optional<boost::property_tree::ptree>();
        }
        return boost::optional<boost::property_tree::ptree>(opt.value());

    }


//------------------------------------------------------------------------------
    static bool has_doc(const std::string & collection, const std::string & doc_name)
    {
        using namespace boost;
        std::shared_lock lk(instance().m_mutex);
        return  filesystem::exists(build_doc_path(collection, doc_name));
    }

//------------------------------------------------------------------------------
    static bool new_doc(const std::string & collection, const std::string & doc_name)
    {
        using namespace boost;
        std::lock_guard lk(instance().m_mutex);

        std::string path(build_doc_path(collection, doc_name));
        if(filesystem::exists(path))
        {
            return false;
        }

        property_tree::ptree obj;
        obj.put("name", doc_name);

        std::stringstream ss;

        property_tree::json_parser::write_json(ss, obj);

        return write_file(ss.str(), path);
    }

//------------------------------------------------------------------------------
    template<typename T>
    static bool put(
            const std::string & collection,
            const std::string & doc_name,
            const std::string & dotted_path,
            const T & value)
    {
        using namespace boost;
        std::lock_guard lk(instance().m_mutex);

        std::string path(build_doc_path(collection, doc_name));

        if(!filesystem::exists(path))
        {
            return false;
        }

        std::string json_str(get_file(path));

        std::stringstream ss(json_str);

        property_tree::ptree obj;
        property_tree::json_parser::read_json(ss, obj);

        obj.put(dotted_path, value);

        if(!property_tree::json_parser::verify_json(obj, 100))
        {
            return false;
        }

        std::stringstream oss;
        property_tree::json_parser::write_json(oss, obj);

        return write_file(oss.str(), path);

    }

//------------------------------------------------------------------------------
   static bool put_child(
                    const std::string & collection,
                    const std::string & doc_name,
                    const std::string & dotted_path,
                    const boost::property_tree::ptree & child)
    {
        using namespace boost;
        std::lock_guard lk(instance().m_mutex);

        std::string path(build_doc_path(collection, doc_name));

        if(!filesystem::exists(path))
        {
            return false;
        }

        std::string json_str(get_file(path));

        std::stringstream ss(json_str);

        property_tree::ptree obj;
        property_tree::json_parser::read_json(ss, obj);


        obj.put_child(dotted_path, child);


        if(!property_tree::json_parser::verify_json(obj, 100))
        {
            return false;
        }

        std::stringstream oss;
        property_tree::json_parser::write_json(oss, obj);

        return write_file(oss.str(), path);

    }


//------------------------------------------------------------------------------
    static bool atomic_modify(
            const std::string & collection,
            const std::string & doc_name,
            const std::function<bool(boost::property_tree::ptree &)> & modifyer
            )
    {
        using namespace boost;
        std::lock_guard lk(instance().m_mutex);

        std::string path(build_doc_path(collection, doc_name));

        if(!filesystem::exists(path))
        {
            return false;
        }

        std::string json_str(get_file(path));

        std::stringstream ss(json_str);

        property_tree::ptree obj;
        property_tree::json_parser::read_json(ss, obj);

        if(!modifyer(obj))
        {
            return false;
        }

        std::ostringstream iss;

        property_tree::json_parser::write_json(iss, obj);

        return write_file(iss.str(), path);

    }

private:


    FSDB()
    {
        init();
    }


    static std::string build_doc_path(
            const std::string & collection,
            const std::string & name)
    {
        std::string path(collections_path);
                path.append("/")
                .append(collection)
                .append("/")
                .append(name);

        return path;
    }

    static bool write_file(const std::string & data, const std::string & name)
    {
        std::ofstream file(name);
        if(!file.is_open())
        {
            return false;
        }

        file.write(data.data(), data.size());
        file.close();
        return  true;
    }

    static std::string get_file(const std::string path)
    {
        std::string json_str;
        file_reader reader(path);
        reader.read_file(json_str);
        return json_str;
    }

    static bool make_dir_if_not_exists(const std::string & path)
    {
        using namespace boost;
        bool res = false;
        if(!filesystem::exists(path))
        {
            filesystem::create_directory(path);
            res = true;
        }

        if(!filesystem::is_directory(path))
        {
            throw std::runtime_error("'" + path + "' is not a directory");
        }

        return res;

    }

    static void make_root_dir()
    {
        make_dir_if_not_exists(std::string(root_path));
    }


    static void make_collections_dir()
    {
        make_dir_if_not_exists(std::string(collections_path));
    }

    static void print_collections()
    {
        using namespace boost;

        std::string output = "[FSDB] Collections:\n";

        filesystem::path path = filesystem::path(std::string(collections_path));

        for(filesystem::directory_entry & dir_entry :
            filesystem::directory_iterator(path))
        {
           output += dir_entry.path().string();
           output += '\n';
        }

        std::cout << output;

    }

    void init()
    {
        std::cout << "[FSDB] INIT...\n";
        make_root_dir();
        make_collections_dir();
        print_collections();
        std::cout << "[FSDB] INITED\n";
    }

    static FSDB & instance()
    {
        static FSDB me;
        return me;
    }

private:

    static constexpr std::string_view root_path = "./FSDB";
    static constexpr std::string_view collections_path = "./FSDB/collections";

    std::shared_mutex m_mutex;

};

#endif // FSDB_H
