
#ifndef FILE_READER_H
#define FILE_READER_H
#include <boost/filesystem.hpp>


#include <fstream>

class file_reader
{
public:
    file_reader(const std::string & file_path)
        : m_file_path(file_path)
    {
    }

    bool read_file(std::string & file_data)
    {
        std::ifstream f(m_file_path);
        if(!f.is_open())
        {
            printf("[FILE READER]: Can't open");
            return false;
        }

        file_data.resize(boost::filesystem::file_size(m_file_path));
        f.read(file_data.data(), file_data.size());
        f.close();
        return true;
    }

private:
    std::string m_file_path;
};
#endif // FILE_READER_H

