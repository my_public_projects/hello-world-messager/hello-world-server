#ifndef API_SERVER_H
#define API_SERVER_H


#include <list>
#include <memory>

#include "thread_pool.h"
#include "sleeper.hpp"
#include "tcp_server.h"
#include "client.h"
#include "client_handler.h"




//------------------------------------------------------------------------------
class api_server
{
public:

//------------------------------------------------------------------------------
    api_server(int port)
        : m_pool(0),
          m_thread(std::bind(&api_server::worker, this)),
          m_server(port, std::bind(
                       &api_server::socket_handler,
                       this,
                       std::placeholders::_1)
                   )
    {
        m_server.start();
        std::cout << "[API SERVER]: Started!\n";
    }
//------------------------------------------------------------------------------

    ~api_server()
    {
        m_is_run = false;
        m_thread.join();
    }


private:

//------------------------------------------------------------------------------
    void handle_c(const std::shared_ptr<client> & client)
    {
        client_handler(*client).handle();
        client->busy.store(false);

    }

//------------------------------------------------------------------------------
    void worker()
    {

        Sleeper sleeper(1, 5'000, "[API SERVER]:");

        while(m_is_run)
        {
            sleeper.sleep();
            for(auto it = m_clients.begin(); it != m_clients.end();)
            {

                if(std::atomic_exchange_explicit(&it->get()->busy, true,
                                                 std::memory_order_acquire))
                {
                    ++it;
                    continue;
                }

                if(it->get()->is_alive)
                {
                    m_pool.submit(std::bind(&api_server::handle_c, this, *it));
                } else
                {
                    std::cout << "[API SERVER]:DISCONNECT\n";
                    it = m_clients.erase(it);
                    continue;
                }

                ++it;

            }

            auto new_c = m_accepted_clients.try_pop();

            if(new_c)
            {
                m_clients.push_back(std::make_shared<client>(*new_c));
            }

        }
    }

//------------------------------------------------------------------------------
    void socket_handler(std::shared_ptr<boost::asio::ip::tcp::socket> sock_ptr)
    {
        std::cout << "[TCP]:Connect " + sock_ptr->remote_endpoint().address().to_string() +
                     ':' + std::to_string(sock_ptr->remote_endpoint().port()) + '\n';

        m_accepted_clients.push(sock_ptr);
    }

private:
    multithreading::thread_pool m_pool;

    multithreading::queue<std::shared_ptr<boost::asio::ip::tcp::socket>>
    m_accepted_clients;

    std::list<std::shared_ptr<client>> m_clients;

    bool m_is_run = true;

    std::thread m_thread;

    tcp_server m_server;

};

#endif // API_SERVER_H
