#ifndef CLIENT_H
#define CLIENT_H

#include <boost/asio.hpp>

#include <atomic>
#include <memory>
#include <chrono>

//------------------------------------------------------------------------------
struct client
{
    std::atomic_bool busy = false;
    bool is_alive = true;
    std::shared_ptr<boost::asio::ip::tcp::socket> socket;
    std::chrono::system_clock::time_point last_ping =
            std::chrono::system_clock::now();

    client( std::shared_ptr<boost::asio::ip::tcp::socket> sock)
        : socket(sock)
    {
    }
};
#endif // CLIENT_H
