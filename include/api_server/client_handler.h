#ifndef CLIENT_HANDLER_H
#define CLIENT_HANDLER_H

#include <boost/property_tree/json_parser.hpp>

#include <iostream>

#include "client.h"
#include "jrpc_verfy.h"
#include "request_handler/request_handler.h"


class client_handler
{
public:

    client_handler(client & client)
        : m_client(client)
    {

    }


    void handle()
    {
        using namespace boost;
        std::vector<char> data;

        if(!read(data))
        {
            m_client.is_alive = false;
            return;
            // std::cout<< std::string("[ECHO]:ERROR! ") + ec.message() + '\n';
        }
        if(data.empty())
        {
            if(!ping())
            {
                std::cout << "[CLIENT HANDLER]: ping failed\n";
                m_client.is_alive = false;
            }

            return;
        }


        data.push_back(0);

        property_tree::ptree jrpc;
        if(!parse_jrpc(data, jrpc))
        {
            if(!send("{\"jsonrpc\":\"2.0\", "
                     "\"error\":{\"errorcode\":0, "
                     "\"message\":\"json parse error\"}}\n"))
            {
                m_client.is_alive = false;
            }
            return;
        }

        jrpc_verify verify(jrpc);

        if(!verify.verify())
        {
            if(!send("{\"jsonrpc\":\"2.0\", "
                     "\"error\":{\"errorcode\":1, "
                     "\"message\":\"bad jsonrpc\"}}\n"))
            {
                m_client.is_alive = false;
            }
            return;
        }

        property_tree::ptree response;
        request_handler::handle_request(jrpc, response);

        std::stringstream ss;
        property_tree::json_parser::write_json(ss, response);

        if(!send(ss.str()))
        {
            std::cout << "[CLIENT HANDLER]: Can't send response\n";
            m_client.is_alive = false;
        }

    }


private:

    bool ping()
    {
        static const std::string ping_data =
                "{\"jsonrpc\":\"2.0\", "
                 "\"method\":\"ping\", "
                 "\"params\":{}, "
                 "\"id\":\"fffff\"}";

        auto now = std::chrono::system_clock::now();

        auto dur_delta = now - m_client.last_ping;

        auto delta =
                std::chrono::duration_cast<std::chrono::seconds>(dur_delta);

        if(delta.count() < 5)
        {
            return true;
        }


        if(!send(ping_data))
        {
            return false;
        }

        int countdown = 100;
        std::vector<char> pong;
        while(countdown && pong.empty())
        {
            if(!read(pong))
            {
                return false;
            }

            std::this_thread::sleep_for(std::chrono::milliseconds(1));
            --countdown;
        }

        if(pong.empty())
        {
            return false;
        }

        pong.push_back(0);

        std::cout << pong.data() << std::endl;

        if(ping_data == pong.data())
        {
            m_client.last_ping = std::chrono::system_clock::now();
            return true;
        }

        return false;

    }

    bool send(const std::string & data)
    {
        using namespace boost;
        boost::system::error_code ec;

        m_client.socket->write_some(asio::buffer(data), ec);

        return !ec;

    }

    bool read(std::vector<char> & data)
    {
        boost::system::error_code ec;
        size_t count = m_client.socket->available(ec);
        while(count)
        {
            size_t old_size = data.size();
            data.resize(old_size + count);
            char temp[count];
            count = m_client.socket->read_some(boost::asio::buffer(temp, count), ec);
            if(ec)
            {
                break;
            }
            std::memcpy(data.data() + old_size, temp, count);
            break;
            count = m_client.socket->available(ec);
        }

        return !ec;
    }


    bool parse_jrpc(
            const std::vector<char> & data,
            boost::property_tree::ptree & jrpc)
    {
        std::string str(data.data());
        std::istringstream iss(data.data());
        using namespace boost;
        try {

            property_tree::json_parser::read_json(iss, jrpc);

        } catch (const property_tree::json_parser::json_parser_error & r) {
            std::cout << std::string("[CLIENT HANDLER]: can't parse : ")
                         .append(data.data(), data.size()).append("\n");
            return false;
        }

        return true;

    }


private:

    client & m_client;

};

#endif //CLIENT_HANDLER_H
