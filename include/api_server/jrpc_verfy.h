#ifndef JRPC_VERFY_H
#define JRPC_VERFY_H
#include <boost/property_tree/ptree.hpp>

class jrpc_verify
{
public:

    jrpc_verify(boost::property_tree::ptree & json)
        : m_jrpc(json)
    {
    }

    bool verify()
    {
        return version() && method() && params() && id();
    }

private:

    bool version()
    {
        std::string value;

        try {

           value = m_jrpc.get<std::string>("jsonrpc");

        } catch (const boost::property_tree::ptree_bad_data & e) {
            return false;
        }

        return value == "2.0";
    }

    bool method()
    {
        std::string value;

        try {

           value = m_jrpc.get<std::string>("method");

        } catch (const boost::property_tree::ptree_bad_data & e) {
            return false;
        }
        return true;
    }

    bool params()
    {
        return m_jrpc.get_child_optional("params").has_value();
    }

    bool id()
    {
        std::string value;
        try {

           value = m_jrpc.get<std::string>("id");

        } catch (const boost::property_tree::ptree_bad_data & e) {
            return false;
        }

        return true;

    }


    boost::property_tree::ptree & m_jrpc;
};


#endif // JRPC_VERFY_H
