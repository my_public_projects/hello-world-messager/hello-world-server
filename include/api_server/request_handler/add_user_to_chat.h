#ifndef ADD_USER_TO_CHAT_H
#define ADD_USER_TO_CHAT_H
#include "base_handler.h"
#include "utils.h"
/*
 * {"jsonrpc":"2.0",
 *  "method":"addusertochat",
 *  "params":{
 *          "chatid":"id"
 *          "token":"token",
 *          "user" : "username"
 *  },
 * "id":"1"}
 */


class add_user_to_chat : public base_handler
{
    using json = boost::property_tree::ptree;
public:
    add_user_to_chat(){};
    ~add_user_to_chat() override{}

    void handle_request(
            const boost::property_tree::ptree &request,
            boost::property_tree::ptree &response) override
    {
        auto token = get_param(request, "token");
        auto chat_id = get_param(request, "chatid");
        auto new_user = get_param(request, "username");
        std::string rid = request.get<std::string>("id");

        if(!token || !chat_id || !new_user)
        {
            send_error(
                        2,
                        "bad params",
                        rid,
                        response);
            return;
        }

        if(!authenticate(token))
        {
            send_error(
                        403,
                        "bad token",
                        rid,
                        response);
            return;
        }


        std::string username = get_user_by_seskey(token.value());

        if(!FSDB::has_doc("chats", chat_id.value()))
        {
            send_error(
                        404,
                        "chat not exists",
                        rid,
                        response);
            return;
        }

        if(!FSDB::has_doc("users", new_user.value()))
        {
            send_error(
                        404,
                        "user not exists",
                        rid,
                        response);
            return;
        }

        auto user_pusher = [&new_user] (json & chat) ->bool
        {
            json & users = chat.get_child("users");

            for(auto & exists : users)
            {
                std::string name = exists.second.get_value<std::string>();
                if(name == new_user.value())
                {
                    return false;
                }
            }

            json user;
            user.put_value(new_user.value());
            users.push_back(std::make_pair("", user));
            return true;
        };

        if(!FSDB::atomic_modify("chats", chat_id.value(), user_pusher))
        {
            std::cout << "WTF! can't push user to chat\n";
            send_error(
                        403,
                        "user allready in chat",
                        rid,
                        response
                        );
            return;
        }

        auto chat_pusher = [&chat_id](json & user) ->bool
        {
            json & chats = user.get_child("chats");

            json chat;
            chat.put_value(chat_id.value());

            chats.push_back(std::make_pair("", chat));

            return true;
        };

        if(!FSDB::atomic_modify("users", new_user.value(), chat_pusher))
        {
            throw std::runtime_error("WTF!? can't push chat in user");
        }

        prepare_jrpc_resp(rid, response);

        json result;

        result.put("messages", "ok");

        response.put_child("result", result);

    }
};

#endif // ADD_USER_TO_CHAT_H
