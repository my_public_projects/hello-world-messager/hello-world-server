#ifndef BASE_HANDLER_H
#define BASE_HANDLER_H
#include <boost/property_tree/ptree.hpp>


class base_handler
{
    using json = boost::property_tree::ptree;
public:
    base_handler(){}
    virtual ~base_handler() {}

    virtual void handle_request(
            const boost::property_tree::ptree & request,
            boost::property_tree::ptree & response
            ) = 0;

protected:

    void send_error(int code,
                     const std::string & message,
                     const std::string & id,
                     boost::property_tree::ptree & response
                     )
    {
        response.put("jsonrpc", "2.0");

        boost::property_tree::ptree error;
        error.put("errorcode", code);
        error.put("message", message);
        response.put_child("error", error);

        response.put("id", id);
    }


    void prepare_jrpc_resp(const std::string & id,
                           boost::property_tree::ptree & response)
    {
        response.put("jsonrpc", "2.0");
        response.put("id", id);
    }


    std::optional<std::string> get_param(
            const json & jrpc,
            const std::string & p_name)
    {
        const json & params = jrpc.get_child("params");

        auto param_bopt = params.get_child_optional(p_name);
        if(!param_bopt.has_value())
        {
            return std::nullopt;
        }


        std::string value;
        try {

            value = param_bopt->get_value<std::string>();

        } catch (const boost::property_tree::ptree_bad_data & e) {
            return std::nullopt;
        }

        return value;

    }


};
#endif // BASE_HANDLER_H
