#ifndef CREATE_CHAT_H
#define CREATE_CHAT_H
#include "base_handler.h"
#include "utils.h"
/*
 * {"jsonrpc":"2.0",
 *  "method":"createchat",
 *  "params":{
 *          "name":"name"
 *          "token":"token"
 *  },
 * "id":"1"}
 */


class create_chat : public base_handler
{
    using json = boost::property_tree::ptree;
public:
    create_chat(){};
    ~create_chat() override{}

    void handle_request(
            const boost::property_tree::ptree &request,
            boost::property_tree::ptree &response) override
    {
        auto token = get_param(request, "token");
        auto chat_name = get_param(request, "name");
        std::string rid = request.get<std::string>("id");

        if(!token || !chat_name)
        {
            send_error(
                        2,
                        "bad params",
                        rid,
                        response);
            return;
        }

        if(!authenticate(token))
        {
            send_error(
                        403,
                        "bad token",
                        rid,
                        response);
            return;
        }


        std::string username = get_user_by_seskey(token.value());

        std::hash<std::string> hasher;
        std::string new_chat_id =
                std::to_string(hasher(username)) +
                std::to_string(hasher(chat_name.value()));

        if(FSDB::has_doc("chats", new_chat_id))
        {
            send_error(
                        505,
                        "chat allready exists",
                        rid,
                        response);
            return;
        }

        if(!FSDB::new_doc("chats", new_chat_id))
        {
            throw std::runtime_error("Can't create new chat");
        }

        auto chat_init = [&chat_name, &username](json & chat) -> bool
        {
            chat.put("name", chat_name.value());

            json users;

            json user;
            user.put_value(username);

            users.push_back(std::make_pair("", user));

            chat.put_child("users", users);

            json messages;

            json init_mess;

            init_mess.put("owner", username);
            init_mess.put("time", str_now());
            init_mess.put("data",
                          username +
                          " create chat " +
                          chat_name.value());

            messages.push_back(std::make_pair("", init_mess));

            chat.put_child("messages", messages);

            return true;

        };

        if(!FSDB::atomic_modify("chats", new_chat_id, chat_init))
        {
            throw std::runtime_error("WTF!? atomic modigfy fail");
        }


        json chat_rec;
        chat_rec.put_value(new_chat_id);

        auto chat_pusher = [new_chat_id, &chat_rec](json & user) ->bool
        {

            json & chats = user.get_child("chats");
            chats.push_back(std::make_pair("", chat_rec));
            return true;
        };

        if(!FSDB::atomic_modify("users", username, chat_pusher))
        {
            throw std::runtime_error("WTF!? atomic modigfy fail");
        }


        prepare_jrpc_resp(rid, response);

        json result;

        result.put("status", "ok");

        response.put_child("result", result);

    }

};


#endif // CREATE_CHAT_H
