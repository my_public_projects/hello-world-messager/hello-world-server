#ifndef GET_CHAT_H
#define GET_CHAT_H
#include "base_handler.h"
#include "utils.h"
/*
 * {"jsonrpc":"2.0",
 *  "method":"getchat",
 *  "params":{
 *          "id":"id"
 *          "token":"token"
 *  },
 * "id":"1"}
 */


class chat_messages : public base_handler
{
    using json = boost::property_tree::ptree;
public:
    chat_messages(){};
    ~chat_messages() override{}

    void handle_request(
            const boost::property_tree::ptree &request,
            boost::property_tree::ptree &response) override
    {
        auto token = get_param(request, "token");
        auto chat_id = get_param(request, "id");
        std::string rid = request.get<std::string>("id");

        if(!token || !chat_id)
        {
            send_error(
                        2,
                        "bad params",
                        rid,
                        response);
            return;
        }

        if(!authenticate(token))
        {
            send_error(
                        403,
                        "bad token",
                        rid,
                        response);
            return;
        }


        std::string username = get_user_by_seskey(token.value());

        if(!FSDB::has_doc("chats", chat_id.value()))
        {
            send_error(
                        404,
                        "chat not exists",
                        rid,
                        response);
            return;
        }


        json messages =
                FSDB::get_child("chats", chat_id.value(), "messages").value();

        prepare_jrpc_resp(rid, response);

        json result;

        result.put_child("messages", messages);

        response.put_child("result", result);

    }

};
#endif // GET_CHAT_H
