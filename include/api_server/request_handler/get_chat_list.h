#ifndef GET_CHAT_LIST_H
#define GET_CHAT_LIST_H

#include "base_handler.h"
#include "utils.h"

/*
 * {"jsonrpc":"2.0",
 *  "method":"getchatlist",
 *  "params":{
 *          "token":"token",
 *  },
 * "id":"1"}
 */

class get_chat_list : public base_handler
{
    using json = boost::property_tree::ptree;
public:
    get_chat_list(){}
    ~get_chat_list() override {}


    void handle_request(
            const boost::property_tree::ptree &request,
            boost::property_tree::ptree &response) override
    {
        auto token = get_param(request, "token");
        std::string rid = request.get<std::string>("id");

        if(!token)
        {
            send_error(
                        2,
                        "bad params",
                        rid,
                        response);
            return;
        }

        if(!authenticate(token))
        {
            send_error(
                        403,
                        "bad token",
                        rid,
                        response);
            return;
        }

        std::string username = get_user_by_seskey(token.value());

        json chats = get_user_chat_list(username);

        json chats_headers;
        fill_headers(chats_headers, chats);

        prepare_jrpc_resp(rid, response);

        json result;
        result.put_child("chats", chats_headers);

        response.put_child("result", result);
    }

private:

    json get_user_chat_list(const std::string & username)
    {
        auto result = FSDB::get_child("users", username, "chats");
        return result.value();
    }

    void fill_headers(json & headers, const json & id_list)
    {
        for(auto & chat : id_list)
        {
            std::string id =  chat.second.get_value<std::string>();

            if(!FSDB::has_doc("chats", id))
            {
                std::cout << "[get_chat_list]: Error! Chat with id:" + id +
                             "not exists! \n";
                continue;
            }

            std::string chat_name;
            FSDB::get("chats", id, "name", chat_name);

            json chat_header;
            chat_header.put("id", id);
            chat_header.put("name", chat_name);
            headers.push_back(std::make_pair("", chat_header));
        }
    }

};

#endif // GET_CHAT_LIST_H
