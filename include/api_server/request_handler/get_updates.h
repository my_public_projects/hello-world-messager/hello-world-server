#ifndef GET_UPDATES_H
#define GET_UPDATES_H
#include "base_handler.h"
#include "utils.h"
/*
 * {"jsonrpc":"2.0",
 *  "method":"getupdates",
 *  "params":{
 *          "token":"token"
 *  },
 * "id":"1"}
 */


class get_updates : public base_handler
{
    using json = boost::property_tree::ptree;
public:
    get_updates(){};
    ~get_updates() override{}

    void handle_request(
            const boost::property_tree::ptree &request,
            boost::property_tree::ptree &response) override
    {
        auto token = get_param(request, "token");
        std::string rid = request.get<std::string>("id");

        if(!token)
        {
            send_error(
                        2,
                        "bad params",
                        rid,
                        response);
            return;
        }

        if(!authenticate(token))
        {
            send_error(
                        403,
                        "bad token",
                        rid,
                        response);
            return;
        }


        std::string username = get_user_by_seskey(token.value());

        if(!FSDB::has_doc("users", username))
        {
            send_error(
                        404,
                        "bad token",
                        rid,
                        response);
            return;
        }

        json updates;

        auto update_extractor = [&updates](json & user) ->bool
        {

            json & temp  = user.get_child("updates");

            updates = temp;

            while (temp.size()) {
                temp.pop_back();
            }

            return true;
        };

        if(!FSDB::atomic_modify("users", username, update_extractor))
        {
            throw std::runtime_error("WTF!? CAN'T extract updates");
        }

        prepare_jrpc_resp(rid, response);

        json result;

        result.put_child("updates", updates);

        result.put("count", updates.size());

        response.put_child("result", result);

    }

};
#endif // GET_UPDATES_H
