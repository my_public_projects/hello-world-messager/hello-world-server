#ifndef LOGIN_HANDLER_H
#define LOGIN_HANDLER_H

#include <time.h>

#include "base_handler.h"
#include "utils.h"


/*
 * {"jsonrpc":"2.0",
 *  "method":"login",
 *  "params":{
 *          "username":"username",
 *          "password":"password"
 *  },
 * "id":"1"}
 */

class login_handler : public base_handler
{
    using json = boost::property_tree::ptree;
public:
    login_handler(){}
    ~login_handler() override {}

    void handle_request(
            const boost::property_tree::ptree &request,
            boost::property_tree::ptree &response) override
    {
        auto username = get_param(request, "username");
        auto password = get_param(request, "password");
        std::string rid = request.get<std::string>("id");

        if(!username || !password)
        {
            send_error(
                        2,
                        "bad params",
                        rid,
                        response);
            return;
        }

        if(!FSDB::has_doc("users", username.value()))
        {
            send_error(
                        666,
                        "Wrong login or password",
                        rid,
                        response
                        );
            return;
        }

        std::string real_pass = get_real_pass(username.value());
        if(real_pass != password.value())
        {
            send_error(
                        666,
                        "Wrong login or password",
                        rid,
                        response
                        );
            return;
        }

        std::hash<std::string> hasher;
        std::string new_ses_key_raw = username.value() + real_pass;

        new_ses_key_raw += str_now();

        std::string new_ses_key =
                std::to_string(hasher(username.value())) +
                std::to_string(hasher(new_ses_key_raw));

        //TODO handle collison
        if(FSDB::has_doc("sessions", new_ses_key))
        {
            std::cout << "[login handler] SESION COLLISION\n";
            send_error(
                        666,
                        "Shit happens... Try harder :)", //LOL
                        rid,
                        response
                        );
            return;
        }

        //TODO session lifetime
        //TDOD put username in session and check on hadling other methods
        if(!FSDB::new_doc("sessions", new_ses_key))
        {
            std::cout << "[login handler] Can't create doc\n";
            send_error(
                        500,
                        "Server error! Sorry...",
                        rid,
                        response
                        );
            return;
        }

        if(!FSDB::put("sessions", new_ses_key, "username", username.value()))
        {
            std::cout << "[login handler] Can't put username\n";
            send_error(
                        500,
                        "Server error! Sorry...",
                        rid,
                        response
                        );
            return;
        }

        prepare_jrpc_resp(rid, response);

        json result;

        result.put("username", username.value());
        result.put("token", new_ses_key);

        response.put_child("result", result);

    }

private:


    std::string get_real_pass(const std::string & username)
    {
        std::string real_pass;
        if(!FSDB::get("users", username, "password", real_pass))
        {
            throw std::runtime_error("[login handler]: "
                                     "WTF! Where is password field!");
        }

        return real_pass;
    }


};

#endif // LOGIN_HANDLER_H
