#ifndef REGISTRATION_HANDLER_H
#define REGISTRATION_HANDLER_H

#include "base_handler.h"
#include "FSDB/FSDB.h"



/*
 * {"jsonrpc":"2.0",
 *  "method":"registration",
 *  "params":{
 *          "username":"username",
 *          "password":"password"
 *  },
 * "id":"1"}
 */

class registration_handler : public base_handler
{
    using json = boost::property_tree::ptree;
public:
    registration_handler()
    {}

    ~registration_handler() override {}

    void handle_request(
            const boost::property_tree::ptree &request,
            boost::property_tree::ptree &response) override
    {

        auto username = get_param(request, "username");
        auto password = get_param(request, "password");
        std::string rid = request.get<std::string>("id");

        if(!username || !password)
        {
            send_error(
                        2,
                        "bad params",
                        rid,
                        response);
            return;
        }

        if(!validate_username(username.value()))
        {
            send_error(5, "login must be alphabetical only\n"
                          "login must be 8-32 symbols", rid, response);
            return;
        }

        if(!validate_password(password.value()))
        {
            send_error(5, "password must be alphabetical only.\n"
                          "password length must be 8-32 symbols", rid, response);
            return;
        }

        if(FSDB::has_doc("users", username.value()))
        {
            send_error(3, "user allready exists", rid, response);
            return;
        }

        if(!FSDB::new_doc("users", username.value()))
        {
            send_error(4, "Can't create new user", rid, response);
            return;
        }

        auto modifyer = [&password]  (json & doc) -> bool
        {
                doc.put("password", password.value());
                doc.put_child("chats", json());
                doc.put_child("updates", json());
                return true;
        };

        if(!FSDB::atomic_modify("users", username.value(), modifyer))
        {
            throw  std::runtime_error("[registration handler]:WAT A FUCK?");
        }

        prepare_jrpc_resp(rid, response);

        json result;
        result.put("status", "ok");

        response.put_child("result", result);

    }


private:

    bool validate_username(const std::string & username)
    {
        int res = 0;

        res += username.size() >= 8 && username.size() <= 32;

        res += std::count_if(
                    username.begin(),
                    username.end(),
                    [](unsigned char ch)
        {
            return std::isalpha(ch);
        }) == int(username.size());

        return res == 2;

    }

    bool validate_password(const std::string & pass)
    {
        int res = 0;

        res += pass.size() >= 8 && pass.size() <= 32;

        res += std::count_if(
                    pass.begin(),
                    pass.end(),
                    [](unsigned char ch)
        {
            return std::isalpha(ch);
        }) == int(pass.size());

        return res == 2;
    }

};

#endif // REGISTRATION_HANDLER_H
