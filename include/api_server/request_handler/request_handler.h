#ifndef REQUEST_HANDLER_H
#define REQUEST_HANDLER_H


#include "base_handler.h"
#include "registration_handler.h"
#include "login_handler.h"
#include "get_chat_list.h"
#include "create_chat.h"
#include "get_chat.h"
#include "send_message.h"
#include "add_user_to_chat.h"
#include "get_updates.h"

class request_handler
{
public:

    static void handle_request(
            const boost::property_tree::ptree & request,
            boost::property_tree::ptree & response)
    {
        std::string method = request.get<std::string>("method");
        std::string id = request.get<std::string>("id");
        auto it = instance().m_handlers.find(method);
        if(it == instance().m_handlers.end())
        {
            method_not_found(response, id);
            return;
        }

        instance().m_handlers.at(method)->handle_request(request, response);

    }

private:
    request_handler()
    {
        m_handlers["registration"] = std::make_shared<registration_handler>();
        m_handlers["login"] = std::make_shared<login_handler>();
        m_handlers["chatlist"] = std::make_shared<get_chat_list>();
        m_handlers["createchat"] = std::make_shared<create_chat>();
        m_handlers["getchat"] = std::make_shared<chat_messages>();
        m_handlers["sendmessage"] = std::make_shared<send_message>();
        m_handlers["addusertochat"] = std::make_shared<add_user_to_chat>();
        m_handlers["getupdates"] = std::make_shared<get_updates>();
    }

    static request_handler & instance()
    {
        static request_handler me;
        return me;
    }


    static void method_not_found(
            boost::property_tree::ptree & response,
            const std::string & id)
    {
        const std::string err =
                "{\"jsonrpc\":\"2.0\", "
                "\"error\":{\"errorcode\":-1, "
                "\"message\":\"wrong method\"}, \"id\":\"" + id + "\"}";

        std::istringstream ss(err);

        boost::property_tree::json_parser::read_json(ss, response);

    }


private:

    std::map<std::string, std::shared_ptr<base_handler>> m_handlers;

};


#endif // REQUEST_HANDLER_H
