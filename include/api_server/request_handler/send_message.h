#ifndef SEND_MESSAGE_H
#define SEND_MESSAGE_H
#include "base_handler.h"
#include "utils.h"
/*
 * {"jsonrpc":"2.0",
 *  "method":"sendmessage",
 *  "params":{
 *          "chatid":"id"
 *          "token":"token",
 *          "message" : {
 *              "owner": "username",
 *              "time": "2020 11 320 19:58:29",
 *              "data": "username create chat chatname"
 *          }
 *
 *  },
 * "id":"1"}
 */


class send_message : public base_handler
{
    using json = boost::property_tree::ptree;
public:
    send_message(){};
    ~send_message() override{}

    void handle_request(
            const boost::property_tree::ptree &request,
            boost::property_tree::ptree &response) override
    {
        auto token = get_param(request, "token");
        auto chat_id = get_param(request, "chatid");
        auto message = request.get_child_optional("params.message");

        std::string rid = request.get<std::string>("id");

        if(!token || !chat_id || !message)
        {
            send_error(
                        2,
                        "bad params",
                        rid,
                        response);
            return;
        }

        if(!authenticate(token))
        {
            send_error(
                        403,
                        "bad token",
                        rid,
                        response);
            return;
        }


        std::string username = get_user_by_seskey(token.value());
        auto owner = message.value().get_optional<std::string>("owner");

        //TODO VALIDATE TIME and data must be in base64 encoding
        if(!owner ||
           !message.value().get_child_optional("time") ||
           !message.value().get_child_optional("data"))
        {
            send_error(
                        505,
                        "bad format",
                        rid,
                        response);
            return;
        }

        if(owner.value() != username)
        {
            send_error(
                        403,
                        "bad Hacker, bad!",
                        rid,
                        response);
            return;
        }

        if(!FSDB::has_doc("chats", chat_id.value()))
        {
            send_error(
                        404,
                        "chat not exists",
                        rid,
                        response);
            return;
        }

        auto message_pusher = [&message] (json & chat) ->bool
        {
            json & messages = chat.get_child("messages");

            messages.push_back(std::make_pair("", message.value()));
            return true;
        };

        if(!FSDB::atomic_modify("chats", chat_id.value(), message_pusher))
        {
            throw std::runtime_error("WTF! can't push message to chat");
        }

        send_notifications(chat_id.value(), username);

        prepare_jrpc_resp(rid, response);

        json result;

        result.put("messages", "ok");

        response.put_child("result", result);

    }


    bool send_notifications(
                            const std::string & chat_id,
                            const std::string & initiator)
    {
        auto user_list = FSDB::get_child("chats", chat_id, "users");
        if(!user_list)
        {
            return false;
        }

        for(auto & user : user_list.value())
        {
            std::string username = user.second.get_value<std::string>();
            if(username == initiator)
            {
                continue;
            }

            json notyfi;

            notyfi.put("type", "newmessage");
            notyfi.put("chatid", chat_id);

            auto notify_pusher = [&notyfi](json & user) ->bool
            {
                json & updates = user.get_child("updates");

                updates.push_back(std::make_pair("", notyfi));

                return true;
            };

            if(!FSDB::atomic_modify("users", username, notify_pusher))
            {
                throw std::runtime_error("WTF!? Can't push notyfication");
            }
        }
        return true;
    }

};
#endif // SEND_MESSAGE_H
