#ifndef UTILS_H
#define UTILS_H
#include "FSDB/FSDB.h"

inline std::string get_user_by_seskey(const std::string & key)
{
    std::string username;
    if(!FSDB::get("sessions", key, "username", username))
    {
        throw std::runtime_error("[get user by ses]: fail");
    }

    return username;

}


inline bool authenticate(std::optional<std::string> token)
{
    return FSDB::has_doc("sessions", token.value());
}

inline std::string str_now()
{
    std::time_t time_t =
            std::chrono::system_clock::to_time_t(
                    std::chrono::system_clock::now()
                );

    std::string buff;
    buff.resize(100);
    struct tm t;

    localtime_r(&time_t, &t);

    std::strftime(
                buff.data(), 100, "%Y %m %j %T",
                &t);

    while (buff.size() && buff.back() == '\0') {
        buff.pop_back();
    }

    return buff;
}

#endif // UTILS_H
