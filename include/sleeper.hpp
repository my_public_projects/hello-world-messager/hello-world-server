#ifndef SLEEPER_H
#define SLEEPER_H
#include <thread>
class Sleeper
{
    std::chrono::steady_clock::time_point m_prev_sleep;
    size_t m_ms_sleep_time;
    size_t m_us_allowable_delay;
    std::string_view m_not_sleep_message;
public:
    Sleeper(
            size_t ms_sleep_time,
            size_t us_allowable_delay,
            std::string_view not_sleep_message
            )
        : m_prev_sleep(std::chrono::steady_clock::now()),
          m_ms_sleep_time(ms_sleep_time),
          m_us_allowable_delay(us_allowable_delay),
          m_not_sleep_message(not_sleep_message)
    {

    }


    void sleep()
    {
        std::chrono::steady_clock::time_point temp =
                std::chrono::steady_clock::now();
        auto delta = temp - m_prev_sleep;
        auto delta_duration =
                std::chrono::duration_cast<std::chrono::microseconds>(delta);

        if(delta_duration < std::chrono::microseconds(m_us_allowable_delay))
        {
            //logger::instance().info("SLEEP");
            std::this_thread::sleep_for(
                        std::chrono::milliseconds(m_ms_sleep_time));

        }
#ifndef NDEBUG
        else
        {

            printf("%s ->NOT SLEEP %ld usec\n",
                   std::string(m_not_sleep_message).c_str(),
                   delta_duration.count());
        }
#endif
        m_prev_sleep = std::chrono::steady_clock::now();

    }
};

#endif // SLEEPER_H
