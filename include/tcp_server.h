#ifndef TCP_SERVER_H
#define TCP_SERVER_H
#include <iostream>
#include <boost/asio.hpp>

class tcp_server
{
public:
    using handler_function =
    std::function<void(std::shared_ptr<boost::asio::ip::tcp::socket>)>;

    tcp_server(int port,
               const handler_function & sock_handler)
        : m_port(port),
          m_socket_handler(sock_handler)
    {

    }

    ~tcp_server()
    {
        stop();
        if(m_thread.joinable())
        {
            m_thread.join();
        }
    }


    void start()
    {
        if(m_is_run)
        {
            return;
        }
        m_is_run = true;
        m_thread = std::thread(std::bind(&tcp_server::worker, this));
    }

    void stop()
    {
        m_is_run = false;
    }

private:

    void worker()
    {
        using namespace boost;
        std::shared_ptr<asio::ip::tcp::socket> sock_ptr;
        asio::io_service service;
        asio::ip::tcp::endpoint endpoint(asio::ip::tcp::v4(), m_port);
        asio::ip::tcp::acceptor acceptor(service, endpoint);

        while (m_is_run)
        {
            sock_ptr = std::make_shared<asio::ip::tcp::socket>(service);
            try {
                acceptor.accept(*sock_ptr);
            } catch (const boost::system::system_error & e) {
                std::cout << std::string("[TCP SERVER]: ") + e.what() + '\n';
                continue;
            }

            try {
                m_socket_handler(sock_ptr);
            } catch (const std::exception & e)
            {
                std::cout << std::string("[TCP SERVER]: ") + e.what() + '\n';
                m_is_run = false;
                break;
            }
        }

        std::cout << std::string("[TCP SERVER]: STOPED\n");

    }

private:

    bool m_is_run = false;
    std::thread m_thread;
    int m_port = 80;
    handler_function m_socket_handler;

};

#endif // TCP_SERVER_H
