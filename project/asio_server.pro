TEMPLATE = app
QMAKE_CXXFLAGS_GNUCXX11 = -std=c++17
QMAKE_CXXFLAGS_GNUCXX1Z = -std=c++17
CONFIG += c++17
unix: QMAKE_CXXFLAGS += -std=c++17
unix: QMAKE_CXXFLAGS += -Wno-unknown-pragmas
unix: QMAKE_CXXFLAGS += -Wno-unused-parameter
unix: QMAKE_CXXFLAGS += -Wno-sign-compare
unix: QMAKE_CXX = g++-8
CONFIG -= app_bundle
CONFIG -= qt


DEFINES += BOOST_NO_CXX11_SCOPED_ENUMS

TARGET = "tcp_echo_srv"
DESTDIR = $$PWD/../bin/

INCLUDEPATH += $$PWD/../include $$PWD/../../../depend/boost_1_74_0/

LIBS += -lboost_filesystem

LIBS += -lpthread
SOURCES += \
        ../src/main.cpp \
        ../src/thread_pool.cpp

HEADERS += \
    ../include/FSDB/FSDB.h \
    ../include/FSDB/file_reader.h \
    ../include/api_server/api_server.h \
    ../include/api_server/client.h \
    ../include/api_server/client_handler.h \
    ../include/api_server/jrpc_verfy.h \
    ../include/api_server/request_handler/add_user_to_chat.h \
    ../include/api_server/request_handler/base_handler.h \
    ../include/api_server/request_handler/create_chat.h \
    ../include/api_server/request_handler/get_chat.h \
    ../include/api_server/request_handler/get_chat_list.h \
    ../include/api_server/request_handler/get_updates.h \
    ../include/api_server/request_handler/login_handler.h \
    ../include/api_server/request_handler/registration_handler.h \
    ../include/api_server/request_handler/request_handler.h \
    ../include/api_server/request_handler/send_message.h \
    ../include/api_server/request_handler/utils.h \
    ../include/queue.h \
    ../include/sleeper.hpp \
    ../include/tcp_server.h \
    ../include/thread_pool.h
