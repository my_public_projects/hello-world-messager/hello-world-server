#include <iostream>
#include <list>

#include "tcp_server.h"
#include "thread_pool.h"
#include "sleeper.hpp"
#include "FSDB/FSDB.h"
#include "api_server/api_server.h"


int main()
{


    if(!FSDB::has_collection("users"))
    {
        FSDB::new_collection("users");
    }

    if(!FSDB::has_collection("chats"))
    {
        FSDB::new_collection("chats");
    }

    if(!FSDB::has_collection("sessions"))
    {
        FSDB::new_collection("sessions");
    }

    if(!FSDB::has_doc("users", "infernalboy"))
    {
        FSDB::new_doc("users", "infernalboy");
    }

    if(!FSDB::put("users", "infernalboy", "password", std::string("qwerty")))
    {
        throw std::runtime_error("Can't put");
    }

    std::string pass;
    if(!FSDB::get("users", "infernalboy", "password", pass))
    {
        throw std::runtime_error("Can't get");
    }

    std::cout << "[TEST FSDB]: get ->" + pass + "\n";


    using namespace boost;


    property_tree::ptree chats;
    property_tree::ptree item;
    item.put_value(std::string("12341232"));

    chats.push_back(std::make_pair("", item));
    chats.push_back(std::make_pair("", item));
    chats.push_back(std::make_pair("", item));
    chats.push_back(std::make_pair("", item));

    if(!FSDB::put_child("users", "infernalboy", "chats", chats))
    {
        throw std::runtime_error("Can't put array");
    }

    property_tree::ptree e_chats;
    auto opt = FSDB::get_child("users", "infernalboy", "chats");
    if(!opt)
    {
        throw std::runtime_error("Can't get chats");
    }

    std::cout << "[TEST FSDB]: Chats:\n";
    for(auto & chat : opt.value())
    {
        std::cout << chat.first << ':' << chat.second.get_value<std::string>() << std::endl;
    }


    api_server server(8080);

    std::cin.get();

    return 0;
}
